<?php 
/*
************************************************************************************************************
   IMPLEMENTATION OF STACK AND QUEUE FUNCTIONS
************************************************************************************************************
*/
// FUNCTION TO PRINT ARRAY
function print_array($arr){
	echo "Array: [";
	foreach ($arr as $value) {
		echo " ".$value." ";
	}
	echo nl2br("]\n");
}

// FUNCTION TO INSERT ELEMENTS IN QUEUE OR STACK
function insert($arr,$insert_ele) { 
	if(is_array($insert_ele)){
		foreach ($insert_ele as $value) {
			$arr[] = $value;
		}
	} else { $arr[] = $insert_ele; }	
	return $arr;
}

// FUNCTION TO REMOVE ELEMENT FROM QUEUE
function queue_remove($arr) {
	$arr[0] = null;
	foreach ($arr as $value) {
		if($value != null){
			$temp[] = $value; 	
		}
	}
	return $temp;
}

// FUNCTION TO REMOVE ELEMENT FROM STACK
function stack_remove($arr) {
	$arr[count($arr)-1] = null;
	foreach ($arr as $value) {
		if($value != null){
			$temp[] = $value; 	
		}
	}
	return $temp;
}

//-------------QUEUE----------------- 
$array = [];
echo nl2br("QUEUE IMPLEMENTATION\n");

echo nl2br("Queue Insertion:\n");
$array = insert($array,array(5,6,8,7,1));
print_array($array);
$array = insert($array,10);
print_array($array);

echo nl2br("Queue Deletion:\n");
$array = queue_remove($array);
print_array($array);
echo nl2br("\n");
echo nl2br("\n");

//-------------STACK-----------------
$array = [];
echo nl2br("STACK IMPLEMENTATION\n");

echo nl2br("Stack Insertion:\n");
$array = insert($array,array(8,4,9,2,3));
print_array($array);
$array = insert($array, 55);
print_array($array);	

echo nl2br("Stack Deletion:\n");
$array = stack_remove($array);
print_array($array);	

?>
